const express  = require('express');
const http     = require('http');
const socketio = require('socket.io');


const app    = express();
const server = http.createServer(app);
const io     = socketio(server);

app.use(express.static('public'));
app.use('/phaser', express.static('node_modules/phaser/dist'));

server.listen(3000);


